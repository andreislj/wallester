package main

import (
	"log"
	"net/http"
)

func main() {

	log.Println("Server started on: http://localhost:8081")
	http.HandleFunc("/show", Show)

	http.HandleFunc("/new", New)
	http.HandleFunc("/insert", Insert)

	http.HandleFunc("/edit", Edit)

	s := &http.Server{Addr: ":8081"}
	log.Fatal(s.ListenAndServe())

}
