package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

type Customer struct {
	Id        int
	FirstName string
	LastName  string
}

func dbConn() (db *sql.DB) {
	dbHost := "localhost"
	dbPort := 5432
	dbDriver := "postgres"
	dbUser := "wallester"
	dbPass := "wallester"
	dbName := "wallester"
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPass, dbName)
	db, err := sql.Open(dbDriver, psqlInfo)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func insert(customer Customer) Customer {
	db := dbConn()
	insForm, err := db.Prepare("INSERT INTO customer(first_name, last_name) VALUES($1, $2)")
	if err != nil {
		panic(err.Error())
	}
	//todo: get customer id
	insForm.Exec(customer.FirstName, customer.LastName)
	defer db.Close()

	return customer
}

func update(customer Customer) /*Customer */ {

	ctx := context.Background()
	db := dbConn()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}
	sql, err := db.Prepare("UPDATE customer SET first_name = $1, last_name = $2 WHERE id = $3")
	if err != nil {
		panic(err.Error())
	}

	_, err = sql.ExecContext(ctx, customer.FirstName, customer.LastName, customer.Id)

	if err != nil {
		//log.Fatal(err)
		tx.Rollback()
		fmt.Println("\n", (err), "\n ....Transaction rollback!")
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("....Transaction committed")
	}
}

func findAll() []Customer {
	db := dbConn()
	selDB, err := db.Query("SELECT * FROM customer ORDER BY id DESC")
	if err != nil {
		panic(err.Error())
	}
	customer := Customer{}
	customers := []Customer{}
	for selDB.Next() {
		var id int
		var firstName, lastName string
		err = selDB.Scan(&id, &firstName, &lastName)
		if err != nil {
			panic(err.Error())
		}
		customer.Id = id
		customer.FirstName = firstName
		customer.LastName = lastName
		customers = append(customers, customer)
	}

	defer db.Close()

	return customers
}

func findById(id int) Customer {
	db := dbConn()
	selDB, err := db.Query("SELECT * FROM customer where id = $1", id)
	if err != nil {
		panic(err.Error())
	}
	customer := Customer{}
	for selDB.Next() {
		var id int
		var firstName, lastName string
		err = selDB.Scan(&id, &firstName, &lastName)
		if err != nil {
			panic(err.Error())
		}
		customer.Id = id
		customer.FirstName = firstName
		customer.LastName = lastName
	}

	defer db.Close()

	return customer
}
