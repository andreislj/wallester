SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'wallester' AND pid <> pg_backend_pid();

DROP DATABASE wallester;
DROP ROLE wallester;


CREATE ROLE wallester LOGIN PASSWORD 'wallester';
CREATE DATABASE wallester WITH OWNER = wallester ENCODING 'UTF-8' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;