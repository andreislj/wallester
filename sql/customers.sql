DROP TABLE IF EXISTS customer;

CREATE TABLE customer
(
--     id         bigint NOT NULL PRIMARY KEY,
    id         serial NOT NULL PRIMARY KEY,
    first_name varchar(100),
    last_name  varchar(100)
);

insert into customer values (1, 'Vasja', 'Pupkin');
insert into customer values (2, 'Vladimir', 'Putin');

SELECT setval('customer_id_seq', 100, true);