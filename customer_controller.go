package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

var funcMap = template.FuncMap{
	"increment": func(i int) int {
		return i + 1
	},
}

//1.1. Create customer view.
func New(w http.ResponseWriter, r *http.Request) {

	t, err := template.ParseFiles("views/new.gohtml")
	if err != nil {
		fmt.Println(err)
	}

	err = t.Execute(w, map[string]string{})
	if err != nil {
		fmt.Println(err)
	}

}

func Insert(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {

		firstName := r.FormValue("first_name")
		lastName := r.FormValue("last_name")
		fmt.Println(firstName, lastName)

		//customer validator using  struct tags
		//https://github.com/go-playground/validator

		if firstName == "" {

			fmt.Println("First name is empty")

			t, err := template.ParseFiles("views/new.gohtml")
			if err != nil {
				fmt.Println(err)
			}

			err = t.Execute(w, map[string]string{})
			if err != nil {
				fmt.Println(err)
			}

		}

		insert(Customer{
			FirstName: firstName,
			LastName:  lastName})

		http.Redirect(w, r, "/show", 301)
	}

}

//1.2. Edit customer view.
//Please think how to resolve edit conﬂicts to not override changes of other application users who are changing the same user at the same time.
//TODO: optimistic locking?: https://github.com/go-training/go-transaction-example/blob/master/optimistic/optimistic.go
func Edit(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		custId := r.URL.Query().Get("id")
		id, _ := strconv.Atoi(custId)
		customer := findById(id)

		t, err := template.ParseFiles("views/edit.gohtml")

		err = t.Execute(w, map[string]Customer{"customer": customer})
		if err != nil {
			fmt.Println(err)
		}
	} else if r.Method == "POST" {

		customerId, _ := strconv.Atoi(r.FormValue("id"))
		firstName := r.FormValue("first_name")
		lastName := r.FormValue("last_name")

		update(Customer{
			Id:        customerId,
			FirstName: firstName,
			LastName:  lastName})

		http.Redirect(w, r, "/show", 301)

	}

}

//1.4. Show customer view. Display all customer data
func Show(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("show.gohtml").Funcs(funcMap).ParseFiles("views/show.gohtml")

	customers := findAll()

	err = t.Execute(w, map[string][]Customer{"customers": customers})
	if err != nil {
		fmt.Println(err)
	}

}
