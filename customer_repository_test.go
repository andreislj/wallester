package main

import (
	"reflect"
	"runtime/debug"
	"testing"
)

func AssertEqual(t *testing.T, actual interface{}, expected interface{}) {
	if actual == expected {
		return
	}
	debug.PrintStack()
	t.Errorf("Received %v (type %v), expected %v (type %v)", actual, reflect.TypeOf(actual), expected, reflect.TypeOf(expected))
}

func TestFindAll(t *testing.T) {

	customers := findAll()

	AssertEqual(t, 2, len(customers))
}

func TestFindById(t *testing.T) {

	customer := findById(1)

	AssertEqual(t, customer.FirstName, "Vasja")
}

func TestUpdate(t *testing.T) {

	update(Customer{
		Id:        1,
		FirstName: "First",
		LastName:  "Last"})

}
